jQuery(document).ready(function(){
  // JavaScript source code
  var opacity_value = 1;
  var ruch = true;
  var nr_kroku = 0;
  var animationStarted = false;


  $("#myRange").change(function () {

      opacity_value = $("input[type=range]").val();

      $("#animated_object").animate(
          {

              opacity: opacity_value

          }
      );
  });


  $("#mySelect").change(function () {

      if ($("#mySelect option:selected").text() == "KOŁO") {

          //window.alert($("#mySelect option:selected").text());

          $("#animated_object").css("border-radius", "50%");
      }
      else {
          $("#animated_object").css("border-radius", "0%");
      }
     
  });



  function animatedDiv() {
     
      var animationTime = 500;

      switch (nr_kroku) {

          case 0:
              $("#animated_object").animate({ left: '+=400px' }, animationTime, function() {
                nr_kroku = 1;
                tryBeginDivAnimation();
              });
              break;
          case 1:
              $("#animated_object").animate({ top: '+=400px' }, animationTime, function() {
                nr_kroku = 2;
                tryBeginDivAnimation();
              });
              break;
          case 2:
              $("#animated_object").animate({ left: '-=400px' }, animationTime, function() {
                nr_kroku = 3;
                tryBeginDivAnimation();
              });
              nr_kroku = 3;
              break;
          case 3:
              $("#animated_object").animate({ top: '-=400px' }, animationTime, function() {
                nr_kroku = 0;
                tryBeginDivAnimation();
              });
              break;
      }
    }

    function tryBeginDivAnimation() {
      if (animationStarted)
      {
        animatedDiv();
      }
    }

  $("#start_btn").click(function () {
      animationStarted = true;
      // po naci�ni�ciu przycisku musi ruszyc od miejsca gdzie skonczylo
      tryBeginDivAnimation();
  });

  $("#stop_btn").click(function () {
      //$("#animated_object").clearQueue()
      animationStarted = false;
  });
});
